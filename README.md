# HAProxy From Source with Ansible

This Ansible Playbook will automate the installation of the latest stable [HAProxy](http://www.haproxy.org/) from the source and create the respective configuration files based on templates on a CentOS 7 target host.

The current latest version is [2.5.0](http://www.haproxy.org/download/2.5/src/haproxy-2.5.0.tar.gz)

All the configuration templates can be changed to match your needs for an HAProxy deployment. 

Please, make sure to run the playbook in an appropriate test environment first. 

## Before run it

Make sure that SELinux is disabled or in Permissive mode to allow Rsyslog access to HAProxy's chroot directory. 
This access will allow Rsyslog to create the Unix socket that HAproxy will send its logs to.

## Tests

The current playbook was tested with [CentOS 7](https://wiki.centos.org/action/show/Manuals/ReleaseNotes/CentOS7.2009) and [Ansible 2.9.25](https://docs.ansible.com/ansible/2.9/roadmap/ROADMAP_2_9.html)

It is likely to work with other versions but has not yet been tested.

## Usage

Place the target host inside the `haproxy.ini` file and use the `haproxy.yml` Ansible Playbook to install HAProxy 2.5 on the host. 
Use the appropriate ansible options to match your needs.


```bash
ansible-playbook haproxy.yml
```

## If You Need a Different Version

Just updated the value of the variable `downloadlink` inside `group_vars/all.yml`. 

## Contributing
Merge requests are welcome. For major changes, please open an issue first to discuss what you would like to change.
